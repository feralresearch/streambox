function action(type, payload = {}) {
  return { type, payload };
}

export const GET_STREAM_STATUS = "GET_STREAM_STATUS";
export const STREAM_STATUS_RESPONSE = "STREAM_STATUS_RESPONSE";

export const GET_STREAM_KEYS = "GET_STREAM_KEYS";
export const STREAM_KEYS_RESPONSE = "STREAM_KEYS_RESPONSE";
export const getStreamKeys = (payload) => action(GET_STREAM_KEYS, payload);

export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGIN_RESPONSE = "USER_LOGIN_RESPONSE";
export const USER_LOGOUT = "USER_LOGOUT";
export const userLogin = (payload) => action(USER_LOGIN, payload);
export const userLogout = () => action(USER_LOGOUT);

export const CONFIG_GET_TEMPLATE = "CONFIG_GET_TEMPLATE";
export const CONFIG_GET = "CONFIG_GET";
export const CONFIG_GET_RESPONSE = "CONFIG_GET_RESPONSE";
export const CONFIG_UPDATE = "CONFIG_UPDATE";
export const CONFIG_DELETE = "CONFIG_DELETE";
export const CONFIG_GET_TEMPLATE_RESPONSE = "CONFIG_GET_TEMPLATE_RESPONSE";
export const CONFIG_START_STREAM = "CONFIG_START_STREAM";
export const CONFIG_STOP_STREAM = "CONFIG_STOP_STREAM";
export const configStopStream = () => action(CONFIG_STOP_STREAM);
export const configStartStream = (payload) =>
  action(CONFIG_START_STREAM, payload);
export const configGet = (payload) => action(CONFIG_GET, payload);
export const configUpdate = (payload) => action(CONFIG_UPDATE, payload);
export const configDelete = (payload) => action(CONFIG_DELETE, payload);
